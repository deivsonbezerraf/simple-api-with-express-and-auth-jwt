const express = require("express");
const jwt = require("jsonwebtoken");

const app = express();
const SECRET = "fortalezaec";

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const clientes = [
  { nome: "deivson", senha: "123456" },
  { nome: "jessica", senha: "123456" },
];

app.get("/", (req, res) => {
  res.json({ message: "OK!" });
});

function verifyJWT(req, res, next){
    const token = req.headers['x-access-token'];
    
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    
    jwt.verify(token, SECRET, function(err, decoded) {
      if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
      
      // se tudo estiver ok, salva no request para uso posterior
      req.userId = decoded.id;
      next();
    });
}

app.get("/clientes", verifyJWT, (req, res) => {
  res.json({ clientes: clientes });
});

app.post("/login", (req, res) => {
  if (req.body.nome === "deivson" && req.body.senha === "123456") {
    const token = jwt.sign({ userId: 1 }, SECRET, { expiresIn: 300 });
    return res.json({ auth: true, token });
  }
  // 401 = não autorizado
  res.status(401).end();
});

app.post("/logout", (req, res) => {
  res.end();
});

module.exports = app;
